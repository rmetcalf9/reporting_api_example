# reporting_api_example

This repository contains files related to an example company reporting API.

See the requirements documented created [here](./docs/REQUIREMENT.md).

Rather than implement a basic rest API I wanted to explore creating a graphql API server. I have consumed
graphql API's in the past and have always been meaning to investigate how the servers are created.

I found https://www.apollographql.com/blog/graphql/python/complete-api-guide/ as an example which roughly 
matched the problem description so decided to use this as a basis for my solution. My approach was:
 - Implement the example locally
 - Dockerise the example and migrate it to a locally running database
 - Create the graphQL contract that matched the requirements
 - Implement the API's

This file contains technical details on how to run the examples.
I have also included exercise notes [here](./docs/EXERCISE_NOTES.md).

I have included notes on the second batch of work [here](./docs/EXERCISE_NOTES_2.md).

## Running this example

To run this example you will need to have the following installed:
 - docker
 - docker-compose
 - make 

I have tested this example on ubuntu. As it runs using docker it should work on other platforms.

To run the example in this repo checkout the repo and run the following command in the base directoy:
```
make run
```

Sometimes due to the database not always setting up correctly make run will not start correctly.
I have not addressed this bug and simply killing the process and trying again will work.

Once the container starts you should be able to use a browser to access the graphql playground at 
http://127.0.0.1:5000/graphql 
here you can browse the schema and execute queries.

You will find that on a single machine data may persist between runs. This is because docker-compose
will stop the containers but not destroy them. To reset the data run the following:
```
make cleardata
```
(Must be done while the project is not running)

Example queries you can use:
```
query CompanyResult {
  listCompanyResults {
    success
    errors
    company_results {
      company_name
      next_reporting_datetime
      next_reporting_type
      last_reporting_datetime
    }
  }
}
```

```
query GetCompanyResult {
  getCompanyResult(
    company_name: "Test4"
  ) {
    company_result {
      company_name
      next_reporting_datetime
      next_reporting_type
      last_reporting_datetime
    }
    success
    errors
  }
}
```

Example mutations
```
mutation UpsertCompanyResult {
  upsertCompanyResult(
    company_name: "Test4", 
    next_reporting_datetime: "2022-01-14T07:19:30.929712+00:00", 
    next_reporting_type: TIME,
    last_reporting_datetime: "2022-01-14T07:19:30.929712+00:00"
  ) {
    company_result {
      company_name
      next_reporting_datetime
      next_reporting_type
      last_reporting_datetime
    }
    success
    errors
  }
}
```

```
mutation DeleteCompanyResult {
  deleteCompanyResult(
    company_name: "Test4"
  ) {
    company_result {
      company_name
      next_reporting_datetime
      next_reporting_type
      last_reporting_datetime
    }
    success
    errors
  }
}
```

### Contract tests

Before running contract tests you need to build the contract test container:
```
make buildcontracttestcontainer
```
This will only need to be rerun if the contract test dependancies are changed.
Changes to the tests themselves do not require a container rebuild.

You can perform contract tests using the following process:

1. use make run to run a local version
2. use make contracttest to run the tests in another terminal
