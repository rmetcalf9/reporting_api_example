# Requirement

This document contains the requirements for the reporting API example.

## High level Overview

Create a single entity API to allow the persistence of company reporting dates. API
consumers will post a company name, its last reporting date and time and its next reporting
date and maybe time. Company names are unique.

It should be possible to store company data, query a single companies data and query
a list of companies data.

## Consumers

The API should be designed to be consumable by a frontend webapplication as well as other
systems - possibly used by external customers to integrate into their own systems.

## Areas out of scope

 - Pagination - it is assumed that all reults will be returned on a single page and there is no need to consider pagination
 - Security is not considered.
 - Scaling is not considered
 - Multi region/caching is not considered
 - Resilience is not considered as the service will run on a laptop
 - Latency is not considered
 - Consistency - It will be a single node solution and consistancy need not be considered

## Other assumptions

 - All service consumers will be able to see and record the same data
 - Service will be run on a laptop - cloud native solutions are not considered
 - The only required sorting is by next reporting date ascending and descending
 - Where the next reporting date is an interval the end of the interval is used for purposes of sorting 
 - Data should be persisted to a datastore but backup/redundancy need not be considered
 - Volume - it is considered a small example service and capacity need not be considered 

## Fields required

company_data_type
 - company_name -> String
 - last_report -> ISO date in UTC format
 - next_reporting -> next_reporting_date_type
 
next_reporting_date_type
 - value -> String
 - interval -> List of values
 
 The list of values for the next_reporting_date_type.interval is:
  - Closed
  - Month
  - Day
  - Time
  - Before_London_Open
  - After_London_Close
  - Before_Newyork_Open
  - After_Newyork_Close
  - Morning
  - Midday
  - Afternoon
  - Evening
  - Quarterly
 
The value is a string and the meaning of the value depends on the interval.
 

## Questions

 - Do we need to provide delete or update endpoints
 This is a nice to have
 
 
 - Are there any sort orders required other than next reporting date
 Company name and last reporting dates are also nice to have
 
 - When the same company is posted multiple times should the record be overwritten
Yes - updates should be treated as upserts
