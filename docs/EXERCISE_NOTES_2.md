# Exercise Notes 2

These notes discuss the actions taken in the second batch of work on this repo.

## Testing

The first goal to address was the lack of testing. After considering the best way
to add tests I decided to opt for contract test. This is because I am more interested 
in testing the behaviour of the whole system including the graphQL API's, SQLAlchemy,
and the database itself. If the application logic gets complicated I will consider
adding unit tests.

I implemented contract tests by building a custom testing container.

## API Response sort order

The next feature to implement was sorting the company reports by next reporting date.
I decided to implement the simplest reporting date type which is time.

As I have testing in place I took a test first approach and started by creating an
acceptance test which simply used this reporting type.

 - Added enum type to graphql schema next_reporting_type_type and added only TIME
 - Added field next_reporting_sortposition_datetime for db based sorting and populated it

Once this was complete for a reporting type of TIME I added a reporting type of DAY
to account for different reporting types.

## Outstanding actions

Excluding actions from the last exercise if I had more time I would:
 - Come up with test cases around different timezones and end of day reporting.
 - Complete implementing reporting types are per spec

## Task summary

Before starting this task my goto development approach for API’s would have been a 
restful API using flask in python. I had previously consumed graphQL API’s and 
have been hearing a lot about it so I wanted to experience the API creation 
process.

I found a lot of benefits of the approach when compared to rest which reminded
me of my SOAP/XML days:
 - Able to use contract first development
 - Using graphQL schema features such as enums
 - Python libraries nicely incorporate the schema and helped me adopt good 
 internal naming conventions
 - graphQL schema makes documentation easy and graphQL playground ctrl+Space 
 keyboard shortcut makes writing queries easy

After this task my goto for greenfield API developments that require a backend 
instance has changed to graphQL. However I am still a serverless fan and in
cloud ecosystems serverless functions and API gateways do have the advantage.

I think the testing choice of contract testing was correct in this task as it
was important to test the behaviour of the whole system.
