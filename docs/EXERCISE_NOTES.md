# Exercise Notes

The approach taken for this exercise was to explore graphql as opposed to generating a simple 
set of rest API’s. The reasons for this include:

 - GraphQL provides a clear schema with strong typing
 - GraphQL provides flexible queries which can be adapted to consumer needs
 - I had queried GraphQL endpoints previously and wanted to learn about the creation of a GraphQL service
 - GraphQL has wide usage in the industry and should be a consideration for new API projects
 - GraphQL python libraries provide a speedy development process by reducing the amount of required code


The python GraphQL libraries also have GraphQL playground libraries included which means 
that once I have a dockerised application it is relatively easy to provide exercise reviewers 
with a url which will have a documented schema. This helped me ensure the portability of the 
example.

## Style

I have approached this exercise in using a research and development style rather than a 
tdd/production quality approach. There are no automated tests and the testing approach was 
to manually work through a few queries.  This allowed me to explore the GraphQL libraries 
and features in depth. Learnings here could be taken and industrialised into best practises 
for a production quality application.


## Development Highlights

The approach taken for this exercise was a R&D style. Priorities were to understand the example, 
learn the graphQL schema format used, and implement the exercise requirements in the API.

The example provided instructions on how to set up an online postgres database and connected 
itself to that. I wanted exercise reviewers to run the example independently on a laptop so 
the first task was to create a local datastore. I also ensured that the schema was automatically
created to remove the need for one time setup instructions.

Creating the object model was straight forward. I had to leverage the datetime data type and make 
sure the service excepted iso format.

Adding the delete and get single company result endpoints was very quick as it only required 
creating the resolvers and wiring them together. Topics for further investigation include 
learning about default resolvers as I believe there are ways to eliminate the need for resolvers.

## Outstanding actions

Due to time constraints I was not able to implement the business logic. The main missing 
functionality is calculating the next_reporting_datetime based on the type and the rules 
documented in the requirements. This would need to be added along with individual tests 
around each test.

If this were to be a full service the following would still need to be considered:

 - Add test infrastructure to the project (both unit tests and container tests)
 - Implement calculated field for next reporting date
 - Expand schema for next reporting types
 - Implement sorting of schema
 - Hard coded credentials need to be parameterized
 - Adding uswgi and nginx to the container
 - Database schema is created on startup - improve this process
 - Work out where the output container should be stored
 - Create full CD process triggered from source control
