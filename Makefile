

build:
	docker build . -t reporting_api_exmaple

run: build
	docker-compose up

cleardata:
	docker rm reporting_api_example_localdb_1

buildcontracttestcontainer:
	docker build ./contract_tests/. -t reporting_api_exmaple_contracttest

contracttest:
	@echo "Running the contract tests"
	@docker run --network="host" --rm -v ${CURDIR}/contract_tests/test:/test -e APIENDPOINT=http://localhost:5000 -w /test reporting_api_exmaple_contracttest nosetests --rednose
