
from app import db

class CompanyResult(db.Model):
    company_name = db.Column(db.String, primary_key=True)
    next_reporting_datetime = db.Column(db.DateTime)
    next_reporting_sortposition_datetime = db.Column(db.DateTime)
    next_reporting_type = db.Column(db.String)
    last_reporting_datetime = db.Column(db.DateTime)

    def to_dict(self):
        return {
            "company_name": self.company_name,
            "next_reporting_datetime": self.next_reporting_datetime,
            "next_reporting_type": self.next_reporting_type,
            "last_reporting_datetime": self.last_reporting_datetime
        }
