# This file contains reporting type specific logic
#  The ariadne library maks the graphQL enum type to this type
from .utils import get_date_time_from_iso_string

def next_reporting_type_type_factory(next_reporting_type, next_reporting_datetime):
  if next_reporting_type == "TIME":
    return NextReportingTypeType_time(next_reporting_datetime)
  if next_reporting_type == "DAY":
    return NextReportingTypeType_day(next_reporting_datetime)
  raise ValueError("Unknown next_reporting_type value")

class NextReportingTypeType():
  next_reporting_datetime = None
  def __init__(self, next_reporting_datetime):
    self.next_reporting_datetime = next_reporting_datetime

  def get_sort_position(self):
    raise Exception("Not Overridden")


class NextReportingTypeType_time(NextReportingTypeType):
  def get_sort_position(self):
    return get_date_time_from_iso_string(self.next_reporting_datetime)

class NextReportingTypeType_day(NextReportingTypeType):
  def get_sort_position(self):
    if len(self.next_reporting_datetime) != 10:
      raise ValueError("For Day type you must supply YYYY-MM-DD format")
    # When reporting period is day it is convention to sore items at the end of the period
    date_time = get_date_time_from_iso_string(self.next_reporting_datetime + "T23:23:59.999")
    return date_time
