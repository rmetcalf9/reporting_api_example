import pytz
from dateutil.parser import parse

def get_date_time_from_iso_string(date_time_str):
    dt = parse(date_time_str)
    return dt.astimezone(pytz.utc)

