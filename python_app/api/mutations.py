from ariadne import convert_kwargs_to_snake_case

from api import db
from api.models import CompanyResult
from .utils import get_date_time_from_iso_string
from .next_reporting_type_type import next_reporting_type_type_factory

@convert_kwargs_to_snake_case
def upsert_company_results_resolver(obj, info, company_name, next_reporting_datetime, next_reporting_type, last_reporting_datetime):
    payload = {
        "success": True,
        "company_result": None
    }
    try:
        company_result = CompanyResult.query.get(company_name)

        next_reporting_type_obj = next_reporting_type_type_factory(
            next_reporting_type=next_reporting_type,
            next_reporting_datetime=next_reporting_datetime
        )
        next_reporting_sortposition_datetime = next_reporting_type_obj.get_sort_position()

        if company_result is None:
            company_result = CompanyResult(
                company_name=company_name,
                next_reporting_datetime=next_reporting_datetime,
                next_reporting_type=next_reporting_type,
                last_reporting_datetime=get_date_time_from_iso_string(last_reporting_datetime),
                next_reporting_sortposition_datetime=next_reporting_sortposition_datetime
            )
            db.session.add(company_result)
            payload = {
                "success": True,
                "company_result": company_result.to_dict()
            }
        else:
            # Updating the existing result
            company_result.next_reporting_datetime=next_reporting_datetime
            company_result.next_reporting_type=next_reporting_type
            company_result.last_reporting_datetime=get_date_time_from_iso_string(last_reporting_datetime)
            company_result.next_reporting_sortposition_datetime = next_reporting_sortposition_datetime
            db.session.add(company_result)
            payload = {
                "success": True,
                "company_result": company_result.to_dict()
            }

        db.session.commit()
    except ValueError:  # date format errors
        payload = {
            "success": False,
            "errors": [f"Incorrect date format provided. Dates should be in ISO format"]
        }

    return payload

@convert_kwargs_to_snake_case
def delete_company_results_resolver(obj, info, company_name):
    try:
        company_result = CompanyResult.query.get(company_name)
        if company_result is None:
            raise AttributeError()
        db.session.delete(company_result)
        db.session.commit()
        payload = {"success": True, "company_result": company_result.to_dict()}

    except AttributeError:
        payload = {
            "success": False,
            "errors": ["Not found"]
        }

    return payload
