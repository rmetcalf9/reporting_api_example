from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

# TODO Remove credentials
app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://postgres:docker@localdb:5432/postgres"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

@app.route('/')
def hello():
    return 'My First API !!'