from .models import CompanyResult
from ariadne import convert_kwargs_to_snake_case

def listCompanyResults_resolver(obj, indo, sort_order):
    try:
        results = None

        if sort_order == "COMPANYNAME_ASC":
            results = CompanyResult.query.order_by(CompanyResult.company_name).all()
        elif sort_order == "COMPANYNAME_DESC":
            results = CompanyResult.query.order_by(CompanyResult.company_name.desc()).all()
        elif sort_order == "NEXTREPORTINGDATE_ASC":
            results = CompanyResult.query.order_by(CompanyResult.next_reporting_sortposition_datetime).all()
        elif sort_order == "NEXTREPORTINGDATE_DESC":
            results = CompanyResult.query.order_by(CompanyResult.next_reporting_sortposition_datetime.desc()).all()
        else:
            results = CompanyResult.query.all()
        company_results = [company_result.to_dict() for company_result in results]
        payload = {
            "success": True,
            "company_results": company_results
        }
    except Exception as error:
        payload = {
            "success": False,
            "errors": [str(error)]
        }
    return payload

@convert_kwargs_to_snake_case
def get_company_result_resolver(obj, info, company_name):
    try:
        company_result = CompanyResult.query.get(company_name)
        if company_result is None:
            raise AttributeError()
        payload = {
            "success": True,
            "company_result": company_result.to_dict()
        }

    except AttributeError:  # todo not found
        payload = {
            "success": False,
            "errors": [f"item matching company_name {company_name} not found"]
        }

    return payload

