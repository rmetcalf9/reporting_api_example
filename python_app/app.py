from api import app, db

from ariadne import load_schema_from_path, make_executable_schema, \
    graphql_sync, snake_case_fallback_resolvers, ObjectType, ScalarType, \
    EnumType
from ariadne.constants import PLAYGROUND_HTML
from flask import request, jsonify
from api.queries import listCompanyResults_resolver, get_company_result_resolver
from api.mutations import upsert_company_results_resolver, delete_company_results_resolver

datetime_scalar = ScalarType("Datetime")

@datetime_scalar.serializer
def serialize_datetime(value):
    return value.isoformat()

query = ObjectType("Query")
mutation = ObjectType("Mutation")

query.set_field("listCompanyResults", listCompanyResults_resolver)
query.set_field("getCompanyResult", get_company_result_resolver)

mutation.set_field("upsertCompanyResult", upsert_company_results_resolver)
mutation.set_field("deleteCompanyResult", delete_company_results_resolver)

type_defs = load_schema_from_path("schema.graphql")
schema = make_executable_schema(
    type_defs, query, mutation, snake_case_fallback_resolvers, datetime_scalar
)

@app.route("/graphql", methods=["GET"])
def graphql_playground():
    return PLAYGROUND_HTML, 200


@app.route("/graphql", methods=["POST"])
def graphql_server():
    data = request.get_json()

    success, result = graphql_sync(
        schema,
        data,
        context_value=request,
        debug=app.debug
    )

    status_code = 200 if success else 400
    return jsonify(result), status_code

# Create the schema on startup
db.create_all()
