import unittest
import sample_data
from api_test_calls import upsert_report, verify_report_exists, verify_report_not_exists, delete_report

class test_acceptance_creation_and_deltion(unittest.TestCase):
  def test_create_and_delete_single_report(self):
    graphql_upsert_result = upsert_report(unittestclass = self, report_data=sample_data.get_test_report())
    graphql_company_resultt = graphql_upsert_result["data"]["upsertCompanyResult"]["company_result"]
    verify_report_exists(unittestclass = self,graphql_company_result=graphql_company_resultt)
    delete_report(unittestclass = self,graphql_company_result=graphql_company_resultt)
    verify_report_not_exists(unittestclass = self,graphql_company_result=graphql_company_resultt)
