# This file contains the tests for the time reporting type
import unittest
import sample_data
from api_test_calls import upsert_report, query_all_companies, clear_sample_data, filter_only_sample_data
from datetime import datetime, timedelta
import pytz


class test_acceptance_reporting_type_time(unittest.TestCase):
  def _setup_sample_data_out_of_sequence(self):
    clear_sample_data(unittestclass=self)
    upsert_results = []
    first_reporting_time = datetime.now(pytz.timezone("UTC"))
    next_reporting_time = first_reporting_time + timedelta(days=int(12))

    num_inorder_examples = 10
    for _ in range(0,num_inorder_examples):
      next_reporting_time = next_reporting_time + timedelta(days=int(12))
      upsert_results.append(upsert_report(
        unittestclass=self, report_data=sample_data.get_test_report(next_reporting_time=next_reporting_time.isoformat()))
      )

    # add one more which reports BEFORE all the others to make sure the data isn't added in an accepted order
    upsert_results.append(upsert_report(
      unittestclass=self, report_data=sample_data.get_test_report(next_reporting_time=first_reporting_time.isoformat()))
    )

    return (upsert_results, [num_inorder_examples] + list(range(0,num_inorder_examples)))


  def test_multiple_companies_return_in_correct_next_reporting_date_order(self):
    (upsert_results, expectedOrder) = self._setup_sample_data_out_of_sequence()

    graphql_query_result = filter_only_sample_data(
      unittestclass=self, query_result=query_all_companies(
        unittestclass = self,
        sort_order = "NEXTREPORTINGDATE_ASC"
      )
    )
    company_results = graphql_query_result["data"]["listCompanyResults"]["company_results"]

    self.assertEqual(len(company_results), len(upsert_results))
    counter = 0
    for company_result in company_results:
      self.assertEqual(
        company_result["company_name"],
        upsert_results[expectedOrder[counter]]["data"]["upsertCompanyResult"]["company_result"]["company_name"],
        msg="Companies results returned in wrong order"
      )
      counter += 1

  def test_multiple_companies_return_in_correct_next_reporting_date_order_desc(self):
    (upsert_results, expectedOrder) = self._setup_sample_data_out_of_sequence()

    # In this test we expect the results in a different order
    expectedOrder.reverse()

    graphql_query_result = filter_only_sample_data(
      unittestclass=self, query_result=query_all_companies(
        unittestclass = self,
        sort_order = "NEXTREPORTINGDATE_DESC"
      )
    )
    company_results = graphql_query_result["data"]["listCompanyResults"]["company_results"]

    self.assertEqual(len(company_results), len(upsert_results))
    counter = 0
    for company_result in company_results:
      self.assertEqual(
        company_result["company_name"],
        upsert_results[expectedOrder[counter]]["data"]["upsertCompanyResult"]["company_result"]["company_name"],
        msg="Companies results returned in wrong order"
      )
      counter += 1
