# These tests verify that we can connect to the API

import os
import unittest
import requests

class test_api_conectivity(unittest.TestCase):
  def test_able_to_connect_to_api_endpoint(self):
    self.assertTrue('APIENDPOINT' in os.environ, msg="APIENDPOINT missing from environment")

    url = os.environ['APIENDPOINT'] + '/'
    response = requests.get(url)
    self.assertEqual(response.status_code, 200, msg="Bad response from API")
