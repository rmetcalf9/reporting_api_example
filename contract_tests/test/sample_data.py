# This file allows the creation of sample data for tests
import uuid
import pytz
from datetime import datetime

test_company_name_prefix = "TestCompany-"

def get_test_report(next_reporting_time=datetime.now(pytz.timezone("UTC")).isoformat(), next_reporting_type="TIME", sample_company_name=""):
  return {
    "company_name": test_company_name_prefix + sample_company_name + str(uuid.uuid4()),
    "next_reporting_datetime": next_reporting_time,
    "next_reporting_type": next_reporting_type,
    "last_reporting_datetime": "2022-01-14T07:19:30.929712+00:00"
  }

