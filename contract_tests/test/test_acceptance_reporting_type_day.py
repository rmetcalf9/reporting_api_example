# This file contains the tests for the day reporting type
import unittest
import sample_data
from api_test_calls import upsert_report, query_all_companies, clear_sample_data, filter_only_sample_data
from datetime import datetime, timedelta
import pytz


class test_acceptance_reporting_type_day(unittest.TestCase):
  def test_day_type_sorted_after_time(self):
    clear_sample_data(unittestclass=self)

    # Add 2 time types and one day type and make sure the order is correct
    first_reporting_time = datetime.now(pytz.timezone("UTC"))
    second_reporting_time = first_reporting_time + timedelta(days=int(1))

    third_company_name = upsert_report(
      unittestclass=self, report_data=sample_data.get_test_report(next_reporting_time=second_reporting_time.isoformat(), sample_company_name="third-")
    )["data"]["upsertCompanyResult"]["company_result"]["company_name"]
    second_company_name = upsert_report(
      unittestclass=self, report_data=sample_data.get_test_report(next_reporting_time=first_reporting_time.strftime("%Y-%m-%d"), next_reporting_type="DAY", sample_company_name="second-")
    )["data"]["upsertCompanyResult"]["company_result"]["company_name"]
    first_company_name = upsert_report(
      unittestclass=self, report_data=sample_data.get_test_report(next_reporting_time=first_reporting_time.isoformat(), sample_company_name="first-")
    )["data"]["upsertCompanyResult"]["company_result"]["company_name"]

    graphql_query_result = filter_only_sample_data(
      unittestclass=self, query_result=query_all_companies(
        unittestclass = self,
        sort_order = "NEXTREPORTINGDATE_ASC"
      )
    )
    print(graphql_query_result)

    self.assertEqual(len(graphql_query_result["data"]["listCompanyResults"]["company_results"]),3)
    self.assertEqual(graphql_query_result["data"]["listCompanyResults"]["company_results"][0]["company_name"], first_company_name)
    self.assertEqual(graphql_query_result["data"]["listCompanyResults"]["company_results"][1]["company_name"], second_company_name)
    self.assertEqual(graphql_query_result["data"]["listCompanyResults"]["company_results"][2]["company_name"], third_company_name)

