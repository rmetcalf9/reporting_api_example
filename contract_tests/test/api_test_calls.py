import os
import requests
import json
import logging
import copy
from sample_data import test_company_name_prefix

logging.getLogger("urllib3").setLevel(logging.WARNING)

def call_graphql_api(unittestclass, query, expected_response_status_codes=[200], parse_response=True):
  unittestclass.assertTrue('APIENDPOINT' in os.environ, msg="APIENDPOINT missing from environment")
  url = os.environ['APIENDPOINT'] + '/graphql'

  response = requests.post(url, json={"query": query})
  if not response.status_code in expected_response_status_codes:
    print(" Calling:", url)
    print(" Response:", response.status_code)
    print(" Response Text:", response.text)
    print(" Expected Responses:", expected_response_status_codes)
    unittestclass.assertTrue(False, msg="Incorrect graphQL response received")
  if not parse_response:
    return response
  return json.loads(response.text)

def upsert_report(unittestclass, report_data):
  query = """mutation
  UpsertCompanyResult
  {
    upsertCompanyResult(
      company_name: "%s",
    next_reporting_datetime: "%s",
    next_reporting_type: %s,
    last_reporting_datetime: "%s"
  ) {
    company_result
  {
    company_name
    next_reporting_datetime
    next_reporting_type
    last_reporting_datetime
  }
  success
  errors
  }
  }""" % (report_data["company_name"], report_data["next_reporting_datetime"], report_data["next_reporting_type"], report_data["last_reporting_datetime"])

  return call_graphql_api(unittestclass=unittestclass, query=query)

def query_by_company_name(unittestclass, company_name):
  query = """query GetCompanyResult {
  getCompanyResult(
    company_name: "%s"
  ) {
    company_result {
      company_name
      next_reporting_datetime
      next_reporting_type
      last_reporting_datetime
    }
    success
    errors
  }
  }""" % (company_name)

  return call_graphql_api(unittestclass=unittestclass, query=query)

def verify_report_exists(unittestclass, graphql_company_result):
  company_response = query_by_company_name(unittestclass=unittestclass, company_name = graphql_company_result["company_name"])
  if company_response["data"]["getCompanyResult"]["company_result"] is not None:
    return
  unittestclass.assertTrue(False, msg="Could not find company")

def verify_report_not_exists(unittestclass, graphql_company_result):
  company_response = query_by_company_name(unittestclass=unittestclass, company_name = graphql_company_result["company_name"])
  print(company_response)
  if company_response["data"]["getCompanyResult"]["company_result"] is None:
    return
  unittestclass.assertTrue(False, msg="Found company but it should not exist")

def delete_report(unittestclass, graphql_company_result):
  query = """mutation DeleteCompanyResult {
    deleteCompanyResult(
      company_name: "%s"
    ) {
      company_result {
        company_name
        next_reporting_datetime
        next_reporting_type
        last_reporting_datetime
      }
      success
      errors
    }
  }""" % (graphql_company_result["company_name"])

  return call_graphql_api(unittestclass=unittestclass, query=query)

def query_all_companies(unittestclass, sort_order="null"):
  query = """query CompanyResult {
  listCompanyResults(sort_order: %s) {
    success
    errors
    company_results {
      company_name
      next_reporting_datetime
      next_reporting_type
      last_reporting_datetime
    }
  }
  }""" % sort_order

  return call_graphql_api(unittestclass=unittestclass, query=query)

def filter_only_sample_data(unittestclass, query_result):
  output = copy.deepcopy(query_result)
  output["data"]["listCompanyResults"]["company_results"] = []

  for company_result in query_result["data"]["listCompanyResults"]["company_results"]:
    if company_result["company_name"].startswith(test_company_name_prefix):
      output["data"]["listCompanyResults"]["company_results"].append(company_result)

  # if there are no results none should be output not the empty list
  if output["data"]["listCompanyResults"]["company_results"] == []:
    output["data"]["listCompanyResults"]["company_results"] = None

  return output

def clear_sample_data(unittestclass):
  graphql_query_result = filter_only_sample_data(
    unittestclass=unittestclass,
    query_result=query_all_companies(unittestclass=unittestclass)
  )
  if graphql_query_result["data"]["listCompanyResults"]["company_results"] is None:
    return # there is no sample data to delete

  for company_result in graphql_query_result["data"]["listCompanyResults"]["company_results"]:
    delete_report(unittestclass=unittestclass, graphql_company_result=company_result)


