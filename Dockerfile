FROM python:3.9.9

MAINTAINER Robert Metcalf

ENV APP_DIR /app
ENV FLASK_APP=app.py

EXPOSE 5000

RUN mkdir ${APP_DIR}

COPY ./python_app ${APP_DIR}
RUN pip install -r ${APP_DIR}/requirements.txt
RUN pip install psycopg2-binary

WORKDIR ${APP_DIR}
CMD [ "flask", "run", "--host=0.0.0.0" ]
